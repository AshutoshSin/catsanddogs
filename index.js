var catBtn = document.getElementById('addCat');
var dogBtn = document.getElementById('addDog');
var foxBtn = document.getElementById('addFox');
var startBtn = document.getElementById('start');
var output = '';
var count = 0;

dogBtn.style.display = "none";
foxBtn.style.display = "none";
startBtn.style.display = "none";

catBtn.addEventListener('click', (e) => {
    const API = 'https://aws.random.cat/meow'
    count++;
    if ( count > 3 ) {
        alert('You should add some dogs too !');
        catBtn.style.display = "none";
        dogBtn.style.display = "block";
        document.getElementById('heading').innerHTML = "Love dogs";
        count = 0;
        output += '<br>';
    }
    else {
        fetch(API)
        .then( (rsp) => rsp.json() )
        .then( (data) => {
        output += `
            <img class="image" src="${data.file}" width="200px" height="200px" alt="Cat Image">
            `;
            document.getElementById('output').innerHTML = output;
        })
    }
}); 

dogBtn.addEventListener('click', (e) => {
    const API = 'https://random.dog/woof.json';
    count++;
    if ( count > 3 ) {
        alert('What about some fox !');
        dogBtn.style.display = "none";
        foxBtn.style.display = "block";
        document.getElementById('heading').innerHTML = "Love fox";
        count = 0;
        output += '<br>';
    }
    else {
        fetch(API)
        .then( (rsp) => rsp.json() )
        .then( (data) => {
        output += `
            <img src="${data.url}" width="200px" height="200px alt="Dog Image">
            `;
            document.getElementById('output').innerHTML = output;
        })
    }
});

foxBtn.addEventListener('click', (e) => {
    const API = 'https://randomfox.ca/floof/'
    count++;
    if ( count > 3 ) {
        alert('Thats All!');
        foxBtn.style.display = "none";
        startBtn.style.display = "block";
        document.getElementById('heading').innerHTML = "start again ?";
    }
    else {
        fetch(API)
        .then( (rsp) => rsp.json() )
        .then( (data) => {
        output += `
            <img src="${data.image}" width="200px" height="200px alt="Dog Image">
            `;
            document.getElementById('output').innerHTML = output;
        })
    }
});

startBtn.addEventListener('click', (e) => {
    startBtn.style.display = 'none';
    catBtn.style.display = 'block';
    count = 0;
    output = '';
    document.getElementById('output').innerHTML = '';
    document.getElementById('heading').innerHTML = "Love cats";
});
